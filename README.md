# mmodal-interview
###Assumptions
For this exercise I assumed that the xml would be in a known format. This format is the "item". 
```xml
<item>
    <name>Noodle</name>
    <shape>Rectangle</shape>
    <length>3 feet</length>
    <weight>12 lbs</weight>
    <characteristic>
        <name>Silly</name>
        <score>10</score>
    </characteristic>
    <characteristic>
        <name>Bad</name>
        <score>1</score>
    </characteristic>
</item>
```
I built this application using a combination of Java with Jersey and Angular 2. I used the eclipse IDE 

for the Java components and Visual Studio Code for the JavaScript components. 

These components were developed on a computer running OSX. 

##mmodal-challenge-api
###Build
To build the api using eclipse with maven, I set up a "maven build" run configuration with the goal "clean install"
This goal will package the application as a jar. 

###Run
To run the api using eclipse with maven, I I set up a "maven build" run configuration with with the goal exec:java

Once the application is running it listens at localhost:8080. 

###Endpoints
http://localhost:8080/myapp/format/pipe

Example Output: 
```
Noodle|Rectangle|3 feet|12 lbs|Silly,10.0|Bad,1.0
```

http://localhost:8080/myapp/format/json

Example Output:
```
S_O_I
NAME|Noodle
SHAPE|Rectangle
LENGTH|3 feet
WEIGHT|12 lbs
CHARACTERISTIC|Silly,10.0
CHARACTERISTIC|Bad,1.0
E_O_I
```

http://localhost:8080/myapp/format/key-value

Example Output:
```json
{
    "characteristic": [
        {
            "name": "Silly",
            "score": 10
        },
        {
            "name": "Bad",
            "score": 1
        }
    ],
    "length": "3 feet",
    "name": "Noodle",
    "shape": "Rectangle",
    "weight": "12 lbs"
}
```
All of the endpoints expect a POST request containing the xml in the post request body and that the Content-Type header is set to application/xml. 

###Resources used:
I used the Jersey documentation https://jersey.github.io/ as well as a book about JAX-RS: https://www.amazon.com/RESTful-Java-JAX-RS-2-0-Distributed/dp/144936134X


##mmodal-challenge-gui
###Build
To build and run the GUI, you must have npm, Node and the Angular-CLI installed: https://www.npmjs.com/package/angular2-cli

Once you have npm/node installed cd to the project directory.

From the project directory, type

```
npm install
```

to pull down the node dependencies. 

Once you have all the node dependencies installed, to serve the GUI type
```
ng serve
```


The GUI will be avalible at localhost:4200

###Resources used:
I used the Angular 2 documentation https://angular.io/docs specifically
the pages about HTTP and event handling, I also used the CLI to generate the project boiler plate.

I also used the MDN for information about the HTML5 FileReader. https://developer.mozilla.org/en-US/docs/Web/API/FileReader


##test-data
This folder contains test data for the application(s).

item1.xml and item2.xml can be used in the gui to test uploading of an xml file to the api.

The post man collection can be loaded into postman to test the api, without needing the client GUI. 


