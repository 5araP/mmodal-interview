package com.mmodal.interview.saraprovost.interview_challenge;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 * CORS Filter to allow the GUI to request resources from this API. 
 * the Provider annotation allows this filter to be picked up by Jersey during the server start.
 * I also explicitly added it to the resource config in Main.java 
 * This filter is called before the request is mapped to the appropriate resource,
 * and manipulates the response header(s) to set Access-Control-Allow-Origin. 
 * @author saraprovost
 *
 */
@Provider
public class CORSFilter implements ContainerResponseFilter{

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		responseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
		responseContext.getHeaders().add("Access-Control-Allow-Headers", "Origin, Content-Type, Accept-Encoding, Accept-Language, User-Agent, Accept, Referer, Access-Control-Allow-Origin, Connection, ");
        responseContext.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
		
	}

}
