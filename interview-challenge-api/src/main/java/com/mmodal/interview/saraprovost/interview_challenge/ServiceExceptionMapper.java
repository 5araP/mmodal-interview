package com.mmodal.interview.saraprovost.interview_challenge;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ServiceExceptionMapper implements ExceptionMapper<Exception>{

	@Override
	public Response toResponse(Exception exception) {
		System.out.println(exception.getMessage());
		System.out.println(exception.getCause());
		exception.printStackTrace();
		return Response.status(204).entity(exception.getMessage()).type(MediaType.TEXT_PLAIN).build();
	}

}
