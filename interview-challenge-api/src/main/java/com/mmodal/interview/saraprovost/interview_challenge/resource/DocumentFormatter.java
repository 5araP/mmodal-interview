package com.mmodal.interview.saraprovost.interview_challenge.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mmodal.interview.saraprovost.interview_challenge.model.Item;

/**
 * Main resource class. All formatting endpoints are defined under the "format" collection. 
 * @author saraprovost
 *
 */

@Path("format")
@Consumes(MediaType.APPLICATION_XML)
public class DocumentFormatter {

	
	/**
	 * Pipe delimits the xml. 
	 * @param item
	 * @return
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("pipe")
	public Response formatToText(Item item){
		return Response.status(200).entity(item.pipeDelimit()).build();
	}
	
	/**
	 * Formats the xml as JSON. This is automatically done by the Jersey framework. 
	 * @param item
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("json")
	public Response formatToJSON(Item item){
		return Response.status(200).entity(item).build();
	}
	

	
	/**
	 * Formats the item in key-value format. 
	 * @param item
	 * @return
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("key-value")
	public Response formatToKeyValue(Item item){
		return Response.status(200).entity(item.keyValueFormat()).build();

	}
	
	
}
