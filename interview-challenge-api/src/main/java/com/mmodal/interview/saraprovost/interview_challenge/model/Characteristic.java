package com.mmodal.interview.saraprovost.interview_challenge.model;

/**
 * Nested "Item" element. 
 * @author saraprovost
 *
 */
public class Characteristic {
	
	private String name; //name of the characteristic.
	private double score; //the score of the characteristic. 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	
	
	public String keyValueFormat(){
		StringBuilder sb = new StringBuilder();
		sb.append("  NAME|");
		sb.append(this.name);
		sb.append("\n");
		sb.append("  SCORE|");
		sb.append(this.score);
		sb.append("\n");
		
		return sb.toString();
	}
	public String commaDelmit(){
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append(",");
		sb.append(score);
		
		return sb.toString();
	}
	
}
