package com.mmodal.interview.saraprovost.interview_challenge.model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * "Item" POJO to marshall/unmarshall the xml uploaded. 
 * Contains nested element "characteristics"
 * @author saraprovost
 *
 */

@XmlRootElement(name = "item")
public class Item {
	
	private String name; //name of the item.
	private String shape; //shape of the item.
	private String color; //color of the item. 
	private String length; //length of the item. 
	private String weight; //weight of the item. 
	private ArrayList<Characteristic> characteristic; //list of item characteristics. 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(this.name == null){
			this.name = "";
		}
		this.name = name;
	}
	
	public String getShape() {
		return shape;
	}
	public void setShape(String shape) {
		if(this.shape == null){
			this.shape = "";
		}
		this.shape = shape;
	}
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		if(this.color == null){
			this.color = "";
		}
		this.color = color;
	}
	
	public String getLength() {
		return length;
	}
	public void setLength(String length) {
		if(this.length == null){
			this.length = "";
		}
		this.length = length;
	}
	
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		if(this.weight == null){
			this.weight = "";
		}
		this.weight = weight;
	}
	
	public ArrayList<Characteristic> getCharacteristic() {
		return characteristic;
	}
	public void setCharacteristic(ArrayList<Characteristic> characteristics) {
		this.characteristic = characteristics;
	}
	
	public String pipeDelimit(){
		StringBuilder sb = new StringBuilder();
		
		if(this.name != null){
			sb.append(this.name);
			sb.append("|");
		}
		if(this.shape != null){
			sb.append(this.shape);
			sb.append("|");
		}
		if(this.color != null){
			sb.append(this.color);
			sb.append("|");
		}
		if(this.length != null){
			sb.append(this.length);
			sb.append("|");
		}
		if(this.weight != null){
			sb.append(this.weight);
		}

		if(characteristic != null){
			for(Characteristic c : this.characteristic){
				sb.append("|");
				sb.append(c.commaDelmit());
			}
		}		
		return sb.toString();
	}
	
	public String keyValueFormat(){
		StringBuilder sb = new StringBuilder();
		sb.append("S_O_I");
		sb.append("\n");
		if(this.name != null){
			sb.append("NAME|");
			sb.append(this.name);
			sb.append("\n");
		}
		if(this.shape != null){
			sb.append("SHAPE|");
			sb.append(this.shape);
			sb.append("\n");
		}
		if(this.color != null){
			sb.append("COLOR|");
			sb.append(this.color);
			sb.append("\n");
		}
		if(this.length != null){
			sb.append("LENGTH|");
			sb.append(this.length);
			sb.append("\n");
		}
		if(this.weight != null){
			sb.append("WEIGHT|");
			sb.append(this.weight);
			sb.append("\n");
		}
		if(characteristic != null){
			for(Characteristic c: characteristic){
				sb.append("CHARACTERISTIC|");
				sb.append(c.commaDelmit());
				sb.append("\n");
			}
		}

		sb.append("E_O_I");
		
		return sb.toString();
	}

}
