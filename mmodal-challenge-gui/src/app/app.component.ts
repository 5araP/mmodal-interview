import { Component, ViewChild } from '@angular/core';
import{ HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';
/**
 * App component for GUI part of xml parser. Uses an event handler to watch the file input 
 * for a change. If a change is detected, it passes the file to an HTML 5 file reader
 * then it calls the angular post method to send the contents of the uploaded file to 
 * the xml formatting REST API. 
 * For this I used the Angular 2 documentation, specifically the http modules. 
 * 
 * @export
 * @class AppComponent
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor (private http: Http){}
  title = 'XMLFormatter';

  formattedData;
  jsonFormattedData;
  pipeFormattedData;
  keyValueFormattedData;

  public onUploadJson(event){
    let input = event.target.files;
    let file = input[0];
    this.parseFile(file);
  }

  /**
   * Event handler for the file input. 
   * Creates file reader to extract file contents from the file in the event. 
   * Extracts the body of the response, and binds it to the component variable formattedData 
   * for display. 
   * I used this webpage as reference for the file reader: https://developer.mozilla.org/en-US/docs/Web/API/FileReader
   * @param event 
   */
  private parseFile(file){
    this.formattedData = "";
    let reader = new FileReader();

    if(file instanceof Blob){
      reader.readAsText(file);
    }
    reader.onload = () => {
      var text = reader.result;
      console.log(text);
      if(text.length > 0){
        this.post(text, "http://localhost:8080/myapp/format/pipe").subscribe((res) => {
          console.log(res);
          this.pipeFormattedData = res.text();
        }); 
        this.post(text, "http://localhost:8080/myapp/format/key-value").subscribe((res) => {
          console.log(res);
          this.keyValueFormattedData = res.text();
        }); 
        this.post(text, "http://localhost:8080/myapp/format/json").subscribe((res) => {
          console.log(res);
          this.jsonFormattedData = res.text();
        }); 
      }

    }
    reader.onerror = () => {
      this.formattedData = "An error occurred, file could not be uploaded. "
    }
  }

  /**
   * Call the Angular http post method. 
   * Set the content type to xml. 
   */
  private post(xml, url){
    console.log("Calling Post!");
    var headers = new Headers({'Content-Type':'application/xml', 'Accept': 'application/json, text/plain'});
    var options = new RequestOptions({ headers: headers });
    
    return this.http.post(url, xml, options);
    
  }
}
